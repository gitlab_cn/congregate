import json
import base64
from os.path import dirname
import datetime
from time import time
from requests.exceptions import RequestException

from congregate.helpers.base_class import BaseClass
from congregate.helpers.misc_utils import get_dry_log, get_timedelta, \
    is_error_message_present, safe_json_response, strip_protocol, \
    get_decoded_string_from_b64_response_content, do_yml_sub, strip_scheme
from congregate.helpers.json_utils import json_pretty, read_json_file_into_object, write_json_to_file
from congregate.migration.gitlab.api.projects import ProjectsApi
from congregate.migration.gitlab.api.groups import GroupsApi
from congregate.migration.gitlab.api.users import UsersApi
from congregate.migration.gitlab.groups import GroupsClient
from congregate.migration.gitlab.users import UsersClient
from congregate.helpers.mdbc import MongoConnector
from congregate.helpers.migrate_utils import get_dst_path_with_namespace,  get_full_path_with_parent_namespace, \
    dig, get_staged_projects, get_staged_groups, find_user_by_email_comparison_without_id, add_post_migration_stats, is_user_project
from congregate.helpers.utils import rotate_logs
from congregate.migration.gitlab.api.project_repository import ProjectRepositoryApi


class ProjectsClient(BaseClass):
    def __init__(self):
        self.projects_api = ProjectsApi()
        self.groups_api = GroupsApi()
        self.users_api = UsersApi()
        self.groups = GroupsClient()
        self.users = UsersClient()
        self.project_repository_api = ProjectRepositoryApi()
        super().__init__()

    def get_projects(self):
        with open("{}/data/projects.json".format(self.app_path), "r") as f:
            return json.load(f)

    def connect_to_mongo(self):
        return MongoConnector()

    def root_user_present(self, members):
        for member in members:
            if member["id"] == self.config.import_user_id:
                return True
        return False

    def retrieve_project_info(self, host, token, processes=None):
        if self.config.src_parent_group_path:
            self.multi.start_multi_process_stream_with_args(
                self.handle_retrieving_project,
                self.groups_api.get_all_group_projects(
                    self.config.src_parent_id, host, token, with_shared=False),
                host,
                token,
                processes=processes)
        else:
            self.multi.start_multi_process_stream_with_args(
                self.handle_retrieving_project,
                self.projects_api.get_all_projects(host, token),
                host,
                token,
                processes=processes)

    def handle_retrieving_project(self, host, token, project, mongo=None):
        if not mongo:
            mongo = self.connect_to_mongo()

        error, project = is_error_message_present(project)
        if error or not project:
            self.log.error(f"Failed to list project with response:\n{project}")
        else:
            self.log.info(u"[ID: {0}] {1}: {2}".format(
                project["id"], project["name"], project["description"]))
            project["members"] = [m for m in self.projects_api.get_members(
                project["id"], host, token) if m["id"] != 1]

            mongo.insert_data(f"projects-{strip_protocol(host)}", project)
        mongo.close_connection()

    def add_shared_groups(self, new_id, path, shared_with_groups):
        """Adds the list of groups we share the project with."""
        try:
            self.log.info(f"Migrating project {path} shared with groups")
            for group in shared_with_groups:
                dst_full_path = get_full_path_with_parent_namespace(
                    group["group_full_path"])
                new_gid = self.groups.find_group_id_by_path(
                    self.config.destination_host, self.config.destination_token, dst_full_path)
                if new_gid:
                    data = {
                        "group_access": group["group_access_level"],
                        "group_id": new_gid,
                        "expires_at": group["expires_at"]
                    }
                    r = self.projects_api.add_shared_group(
                        self.config.destination_host, self.config.destination_token, new_id, data)
                    if r.status_code == 201:
                        self.log.info(
                            f"Shared project {path} with group {dst_full_path}")
                    else:
                        self.log.error(
                            f"Failed to share project {path} with group {dst_full_path} due to:\n{r.content}")
                else:
                    self.log.error(
                        f"Failed to find group {dst_full_path} on destination using new ID {new_gid}")
            return True
        except RequestException as re:
            self.log.error(
                f"Failed to POST shared group {dst_full_path} to project {path}, with error:\n{re}")
            return False

    def find_project_by_path(self, host, token, dst_path_with_namespace):
        """Returns the project ID based on search by path."""
        self.log.info(
            f"Searching on {host} for project {dst_path_with_namespace}")
        resp = self.projects_api.get_project_by_path_with_namespace(
            dst_path_with_namespace, host, token)
        if resp.status_code == 200:
            project = safe_json_response(resp)
            if project and (project.get("path_with_namespace",
                                        '').lower() == dst_path_with_namespace.lower()):
                return project.get("id", None)
        return None

    def delete_projects(self, dry_run=True):
        staged_projects = get_staged_projects()
        for sp in staged_projects:
            # SaaS destination instances have a parent group
            path_with_namespace = get_dst_path_with_namespace(sp)
            self.log.info("Removing project {}".format(path_with_namespace))
            resp = self.projects_api.get_project_by_path_with_namespace(
                path_with_namespace,
                self.config.destination_host,
                self.config.destination_token)
            if resp is not None:
                if resp.status_code != 200:
                    self.log.info("Project {0} does not exist (status: {1})".format(
                        path_with_namespace, resp.status_code))
                elif not dry_run:
                    try:
                        project = resp.json()
                        if get_timedelta(
                                project["created_at"]) < self.config.max_asset_expiration_time:
                            self.projects_api.delete_project(
                                self.config.destination_host,
                                self.config.destination_token,
                                project["id"])
                        else:
                            self.log.info("Ignoring {0}. Project existed before {1} hours".format(
                                project["name_with_namespace"], self.config.max_asset_expiration_time))
                    except RequestException as re:
                        self.log.error(
                            "Failed to remove project\n{0}\nwith error:\n{1}".format(json_pretty(sp), re))
            else:
                self.log.error(
                    "Failed to GET project {} by path_with_namespace".format(path_with_namespace))

    def count_unarchived_projects(self, local=False):
        unarchived_user_projects = []
        unarchived_group_projects = []
        for project in (self.get_projects() if local else self.projects_api.get_all_projects(self.config.source_host, self.config.source_token)):
            if not project.get("archived", True):
                unarchived_user_projects.append(project["path_with_namespace"]) if project["namespace"][
                    "kind"] == "user" else unarchived_group_projects.append(project["path_with_namespace"])
        self.log.info("Unarchived user projects ({0}):\n{1}".format(
            len(unarchived_user_projects), "\n".join(up for up in unarchived_user_projects)))
        self.log.info("Unarchived group projects ({0}):\n{1}".format(
            len(unarchived_group_projects), "\n".join(up for up in unarchived_group_projects)))

    def archive_staged_projects(self, dry_run=True):
        start = time()
        rotate_logs()
        staged_projects = get_staged_projects()
        self.log.info("Project count is: {}".format(len(staged_projects)))
        try:
            for project in staged_projects:
                self.log.info("{0}Archiving source project {1}".format(
                    get_dry_log(dry_run),
                    project["path_with_namespace"]))
                if not dry_run:
                    self.projects_api.archive_project(
                        self.config.source_host,
                        self.config.source_token,
                        project["id"])
        except RequestException as re:
            self.log.error(
                "Failed to archive staged projects, with error:\n{}".format(re))
        finally:
            add_post_migration_stats(start, log=self.log)

    def unarchive_staged_projects(self, dry_run=True):
        start = time()
        rotate_logs()
        staged_projects = get_staged_projects()
        self.log.info("Project count is: {}".format(len(staged_projects)))
        try:
            for project in staged_projects:
                self.log.info("{0}Unarchiving source project {1}".format(
                    get_dry_log(dry_run),
                    project["path_with_namespace"]))
                if not dry_run:
                    self.projects_api.unarchive_project(
                        self.config.source_host,
                        self.config.source_token,
                        project["id"])
        except RequestException as re:
            self.log.error(
                "Failed to unarchive staged projects, with error:\n{}".format(re))
        finally:
            add_post_migration_stats(start, log=self.log)

    def filter_projects_by_state(self, archived=False, dry_run=True):
        staged = get_staged_projects()
        self.log.info(f"Total staged projects count: {len(staged)}")
        arch = [s for s in staged if s.get("archived")]
        unarch = [s for s in staged if not s.get("archived")]
        diff = arch if archived else unarch

        self.log.info(
            f"{get_dry_log(dry_run)}Keeping ONLY {'archived' if archived else 'unarchived'} {len(diff)}/{len(staged)} projects staged")
        if not dry_run:
            write_json_to_file(
                f"{self.app_path}/data/staged_projects.json", diff, log=self.log)
        return len(diff)

    def find_unimported_projects(self, dry_run=True):
        unimported_projects = []
        files = self.get_projects()
        if files is not None and files:
            for project in files:
                try:
                    path = project["path_with_namespace"]
                    self.log.info(
                        "Searching for project {} on destination".format(path))
                    project_exists = False
                    for proj in self.projects_api.search_for_project(
                            self.config.destination_host,
                            self.config.destination_token,
                            project['name']):
                        if proj["name"] == project["name"]:
                            if dig(project, 'namespace', 'full_path', default="").lower(
                            ) == proj.get("path_with_namespace", "").lower():
                                project_exists = True
                                break
                    if not project_exists:
                        self.log.info("Adding project {}".format(path))
                        unimported_projects.append(
                            "%s/%s" % (project["namespace"], project["name"]))
                except IOError as ioe:
                    self.log.error(
                        "Failed to find unimported projects, with error:\n{}".format(ioe))

        if unimported_projects is not None and unimported_projects:
            self.log.info("{0}Found {1} unimported projects".format(
                get_dry_log(dry_run),
                len(unimported_projects)))
            if not dry_run:
                with open("{}/data/unimported_projects.txt".format(self.app_path), "w") as f:
                    for project in unimported_projects:
                        f.writelines(project + "\n")

    def find_empty_repos(self):
        empty_repos = []
        dest_projects = self.projects_api.get_all_projects(
            self.config.destination_host,
            self.config.destination_token,
            statistics=True)
        src_projects = self.projects_api.get_all_projects(
            self.config.source_host,
            self.config.source_token,
            statistics=True)
        for dp in dest_projects:
            if dp.get("statistics", None) is not None and dig(
                    dp, 'statistics', 'repository_size', default=-1) == 0:
                self.log.info("Found empty repo on destination instance: {}".format(
                    dp["name_with_namespace"]))
                for sp in src_projects:
                    if sp["name"] == dp["name"] and dig(dp, 'namespace', 'path') in dig(
                            sp, 'namespace', 'path', default=""):
                        self.log.info("Found source project {}".format(
                            sp["name_with_namespace"]))
                        if sp.get("statistics", None) is not None and dig(
                                sp, 'statistics', 'repository_size', default=-1) == 0:
                            self.log.info(
                                "Project is empty in source instance. Ignoring")
                        else:
                            empty_repos.append(dp["name_with_namespace"])
        self.log.info("Empty repositories ({0}):\n{1}".format(
            len(empty_repos), "\n".join(ep for ep in empty_repos)))

    def validate_staged_projects_schema(self):
        staged_groups = get_staged_groups()
        for g in staged_groups:
            self.log.info(g)
            if g.get("name", None) is None:
                self.log.warning("name is missing")
            if g.get("namespace", None) is None:
                self.log.warning("namespace is missing")
            if g.get("project_type", None) is None:
                self.log.warning("project_type is missing")
            if g.get("default_branch", None) is None:
                self.log.warning("default_branch is missing")
            if g.get("visibility", None) is None:
                self.log.warning("visibility is missing")
            if g.get("http_url_to_repo", None) is None:
                self.log.warning("http_url_to_repo is missing")
            if g.get("shared_runners_enabled", None) is None:
                self.log.warning("shared_runners_enabled is missing")
            if g.get("members", None) is None:
                self.log.warning("members is missing")
            if g.get("id", None) is None:
                self.log.warning("id is missing")
            if g.get("description", None) is None:
                self.log.warning("description is missing")

    def add_members_to_destination_project(
            self, host, token, project_id, members):
        result = {}
        self.log.info(
            f"Adding members to project ID {project_id}:\n{json_pretty(members)}")
        for member in members:
            user_id_req = find_user_by_email_comparison_without_id(
                member["email"])
            member["user_id"] = user_id_req.get(
                "id", None) if user_id_req else None
            result[member["email"]] = False
            if member.get("user_id"):
                resp = safe_json_response(self.projects_api.add_member(
                    project_id, host, token, member))
                if resp:
                    result[member["email"]] = True
        return result

    def get_replacement_data(self, data, f, project_id, src_branch):
        """
        :param data: (dict) Data for the replacement task. See form in description
        :param f: (str) The filename we are replacing for. Used for logging in this method
        :param project_id: (int) The project id on the destination
        :param src_branch: (str) The name of the branch we will be pulling files from in the project

        Takes a pattern replace list data item of form:
        'data':
            {
                'pattern': regex pattern string,
                'replace_with': replacement string. can be a ci_var name
            }
        checks pattern and replace_with are valid, and returns them as a tuple. Does the logging on invalid values
        """
        if not data or not isinstance(data, dict) or len(data) == 0:
            self.log.warning(
                f"No replacement data configured for file {f} in project_id {project_id} branch {src_branch}"
            )
            return None
        pattern = data.get("pattern", None)
        if not pattern or not isinstance(
                pattern, str) or pattern.strip() == "":
            self.log.warning(
                f"No pattern configured for file {f} in project_id {project_id} branch {src_branch}"
            )
            return None
        replace_with = data.get("replace_with", None)
        if not replace_with or not isinstance(
                replace_with, str) or replace_with.strip() == "":
            self.log.warning(
                f"No replace_with configured for file {f} in project_id {project_id} branch {src_branch}"
            )
            return None
        return pattern, replace_with

    def migrate_gitlab_variable_replace_ci_yml(self, project_id):
        """
        :param project_id: (int) The project_id at the destination

        Does the pattern replacement in project files, and cuts a branch with the changes
        {
            "filenames": [
                ".gitlab-ci.yml",
                "requirements.yml",
                "ansible/playbooks/requirements.yml",
                "ansible/molecule/default/requirements.yml"
            ],
            "patterns": [
                {
                    "pattern": "https://git.internal.ca/ansible/roles/global-setup.git",
                    "replace_with": "https://gitlab.com/company/infra/ansible/roles/global-setup.git"
                },
                {
                    "pattern": "https://git.internal.ca/ansible/roles/healthcheck.git",
                    "replace_with": "https://gitlab.com/company/infra/ansible/roles/healthcheck.git"
                },
                {...}
            ]
        }
        Notes:
        * Single branch per project, with commits per configured file.
        * Does not group same file names, so if a file is listed twice, instead of once with multiple data entries, it will get two commits on the same new branch.
          * Since the initial read is *always* from the default, this means that the last listed change will "win"
        """
        self.log.info(
            f"Performing URL remapping for destination project id {project_id}")

        create_branch = False
        branch_name = ""
        yml_file = ""
        new_yml_64 = ""

        pattern_list = read_json_file_into_object(
            self.config.remapping_file_path
        )

        # Get the project from destination to get the default branch
        dstn_project = self.projects_api.get_project(
            project_id,
            self.config.destination_host,
            self.config.destination_token
        )

        if dstn_project and dstn_project.status_code == 200:
            dstn_project_json = safe_json_response(dstn_project)
            src_branch = dstn_project_json.get("default_branch", None)
            self.log.info(f"Source branch is {src_branch}")
            if not src_branch or src_branch.strip() == "":
                self.log.warning(
                    f"Could not determine default branch for project_id {project_id}"
                )
                return

        # Get the list of filenames
        filenames = pattern_list.get("filenames", None)
        if not filenames:
            self.log.error(
                "No URL replacement filenames found"
            )
            return

        # Get the list of patterns
        patterns = pattern_list.get("patterns", None)
        if not patterns:
            self.log.error(
                "No URL replacement patterns found"
            )
            return

        for f in filenames:
            if f == "":
                self.log.warning(
                    f"Empty filename in replacement configuration for project_id {project_id} branch {src_branch}"
                )
                continue
            repo_file = f"{f}?ref={src_branch}"
            self.log.info(f"Pulling repository file for rewrite: {repo_file}")
            yml_file_response = self.project_repository_api.get_single_repo_file(
                self.config.destination_host,
                self.config.destination_token,
                project_id,
                f,
                src_branch
            )

            # Content is base64 string
            if yml_file_response is None or (
                    yml_file_response is not None and yml_file_response.status_code != 200):
                self.log.warning(
                    f"No {f} file available for project_id {project_id} branch {src_branch}"
                )
                continue

            if yml_file_response.status_code == 200:
                yml_file = get_decoded_string_from_b64_response_content(
                    yml_file_response
                )
                if not yml_file or yml_file.strip() == "":
                    self.log.warning(
                        f"Empty {f} file found for project_id {project_id} branch {src_branch}"
                    )
                    continue
            elif yml_file_response.status_code == 404:
                self.log.warning(
                    f"No {f} file available for project_id {project_id} branch {src_branch}"
                )
                continue

            # We have the decoded base64
            # Loop of the patterns list
            for p in patterns:
                repl_data = self.get_replacement_data(
                    p, f, project_id, src_branch
                )

                if not repl_data:
                    self.log.warning(
                        f"No replacement data configured for file {f} in project_id {project_id} branch {src_branch}"
                    )
                    continue
                pattern = repl_data[0]
                replace_with = repl_data[1]

                # Perform the substitution
                self.log.info(f"Subbing {pattern} with {replace_with} in {f}")
                subs = do_yml_sub(yml_file, pattern, replace_with)

                # If nothing changed, skip it all
                if subs[1] == 0:
                    self.log.info(
                        f"Found no instances of {pattern} in project_id {project_id} branch {src_branch}"
                    )
                    continue

                #  Log info
                self.log.info(
                    f"Replaced {subs[1]} instances of {pattern} with {replace_with} on project_id {project_id}"
                )
                create_branch = True
                # Make the next pass of the file be with the current subbed
                # value
                yml_file = subs[0]

            # After changing all the data, encode and write
            # b64encode the new data. Must also encode the subs results. This
            # returns a byte object b''
            new_yml_64 = base64.b64encode(yml_file.encode())
            # Put it back to string for the eventual post
            new_yml_64 = new_yml_64.decode()

            # Don't want to create a branch unless we find something to do, and haven't already created one
            # (the name check). For now, place everything on one branch
            # If we want multiple branches, reset create to False *and* empty
            # name
            if create_branch and branch_name == "":
                # Create a branch
                n = datetime.datetime.now()
                branch_name = f"ci-rewrite-{n.year}{n.month}{n.day}{n.hour}{n.minute}{n.second}"
                self.log.info(
                    f"Creating branch {branch_name} in project {project_id} from {src_branch}")
                branch_data = {
                    "branch": branch_name,
                    "ref": src_branch
                }
                branch_create_resp = self.projects_api.create_branch(
                    self.config.destination_host,
                    self.config.destination_token,
                    project_id,
                    data=branch_data
                )
                if not branch_create_resp or (
                        branch_create_resp and branch_create_resp.status_code != 201):
                    self.log.error(
                        f"Could not create branch for regex replace:\nproject: {project_id}\nbranch data: {branch_data}")
                else:
                    create_branch = False

            if branch_name != "":
                # Put the new file
                put_file_data = {
                    "branch": f"{branch_name}",
                    "content": f"{new_yml_64}",
                    "encoding": "base64",
                    "commit_message": f"Commit for migration regex replace replacing file {f}"
                }
                put_resp = self.project_repository_api.put_single_repo_file(
                    self.config.destination_host,
                    self.config.destination_token,
                    project_id,
                    f"{f}",
                    put_file_data
                )
                if not put_resp or (put_resp and put_resp.status_code == 400):
                    self.log.error(
                        f"Could not put commit for regex replace:\nproject: {project_id}\nbranch name: {branch_name}\nfile: {f}")
                branch_name = ""

            # A branch_name reset would need to go here for the multiple
            # branches

    def create_staged_projects_structure(self, dry_run=True, disable_cicd=False):
        start = time()
        rotate_logs()
        staged_projects = get_staged_projects()
        host = self.config.destination_host
        token = self.config.destination_token
        for s in staged_projects:
            try:
                path_with_namespace = s.get("path_with_namespace")
                dst_grp_full_path = get_full_path_with_parent_namespace(
                    dirname(path_with_namespace))
                dst_grp = self.groups.find_group_by_path(
                    host, token, dst_grp_full_path)
                if dst_grp:
                    dst_gid = dst_grp.get("id")
                else:
                    self.log.error(
                        f"SKIP: Parent group {dst_grp_full_path} NOT found")
                    continue
                dst_path = get_dst_path_with_namespace(s)
                dst_pid = self.find_project_by_path(host, token, dst_path)
                if dst_pid:
                    self.log.error(
                        f"SKIP: Project {dst_path} (ID: {dst_pid}) already exists")
                    continue
                name = s.get("name")
                data = {
                    "name": name,
                    "path": s.get("path"),
                    "namespace_id": dst_gid,
                    "visibility": s.get("visibility"),
                    "description": s.get("description"),
                    "default_branch": s.get("default_branch")
                }
                if disable_cicd:
                    data["jobs_enabled"] = False
                    data["shared_runners_enabled"] = False
                    data["auto_devops_enabled"] = False
                self.log.info(
                    f"{get_dry_log(dry_run)}Create {dst_path} empty project structure, with payload {data}")
                if not dry_run:
                    resp = self.projects_api.create_project(
                        host, token, name, data=data)
                    if resp.status_code == 201 and s.get("merge_requests_template"):
                        self.projects_api.edit_project(host, token, safe_json_response(resp).get(
                            "id"), {"merge_requests_template": s["merge_requests_template"]})
                    elif resp.status_code != 201:
                        self.log.error(
                            f"Failed to create and edit project {dst_path}, with response:\n{resp} - {resp.text}")
            except RequestException as re:
                self.log.error(
                    f"Failed to create project {path_with_namespace} with error:\n{re}")
                continue
        add_post_migration_stats(start, log=self.log)

    def push_mirror_staged_projects(self, disabled=False, overwrite=False, force=False, dry_run=True):
        start = time()
        rotate_logs()
        staged_projects = get_staged_projects()
        host = self.config.destination_host
        token = self.config.destination_token
        username = safe_json_response(
            self.users_api.get_current_user(host, token)).get("username", None)
        for s in staged_projects:
            try:
                dst_pid, mirror_path = self.find_mirror_project(s, host, token)
                if dst_pid and mirror_path and username:
                    data = {
                        # username:token is SaaS specific. Revoking the token breaks the mirroring
                        "url": f"{strip_scheme(host)}://{username}:{token}@{strip_protocol(host)}/{mirror_path}.git",
                        "enabled": not disabled,
                        "keep_divergent_refs": not overwrite
                    }
                else:
                    continue
                self.log.info(
                    f"{get_dry_log(dry_run)}Create project {dst_pid} push mirror {mirror_path}")
                if not dry_run:
                    resp = self.projects_api.create_remote_push_mirror(
                        dst_pid, host, token, data=data)
                    if resp.status_code != 201:
                        self.log.error(
                            f"Failed to create project {dst_pid} push mirror to {mirror_path}, with response:\n{resp} - {resp.text}")
                    elif force:
                        # Push commit (skip-ci) to new branch and delete branch
                        self.trigger_mirroring(host, token, s, dst_pid)
            except RequestException as re:
                self.log.error(
                    f"Failed to create project {s.get('path_with_namespace')} push mirror, with error:\n{re}")
                continue
        add_post_migration_stats(start, log=self.log)

    def trigger_mirroring(self, host, token, staged_project, pid):
        branch = "mirroring-trigger"
        commit_data = {
            "branch": branch,
            "commit_message": f"{branch} [skip-ci]",
            # retry in case of main (as of 14.0)
            "start_branch": staged_project.get("default_branch", "master"),
            "actions": [
                {
                    "action": "create",
                    "file_path": f"{branch}.txt",
                    "content": branch
                }
            ]
        }
        try:
            commit_resp = self.project_repository_api.create_commit_with_files_and_actions(
                host, token, pid, data=commit_data)
            if commit_resp.status_code != 201:
                self.log.error(
                    f"Failed to commit branch {branch} to project {pid}, with payload {commit_data}")
            else:
                self.projects_api.delete_branch(
                    host, token, pid, branch)
        except RequestException as re:
            self.log.error(
                f"Failed to commit branch {branch} payload {commit_data} to project {pid}, with error:\n{re}")

    def toggle_staged_projects_push_mirror(self, disable=False, dry_run=True):
        start = time()
        rotate_logs()
        staged_projects = get_staged_projects()
        host = self.config.destination_host
        token = self.config.destination_token
        for s in staged_projects:
            try:
                # Find the relevant push mirror to toggle
                dst_pid, mirror_path = self.find_mirror_project(s, host, token)
                if dst_pid and mirror_path:
                    url = host + "/" + mirror_path
                    mirrors = self.projects_api.get_all_remote_push_mirrors(
                        dst_pid, host, token)
                    mirror_id = None
                    for m in mirrors:
                        if m and m.get("url") == url:
                            mirror_id = m.get("id")
                            data = {
                                "mirror_id": mirror_id,
                                "enabled": not disable
                            }
                    if not mirror_id:
                        self.log.error(
                            f"SKIP: Project {dst_pid} push mirror to {url} NOT found")
                        continue
                else:
                    continue
                self.log.info(
                    f"{get_dry_log(dry_run)}Toggle project {dst_pid} push mirror {mirror_path}, with payload {data}")
                if not dry_run:
                    resp = self.projects_api.edit_remote_push_mirror(
                        dst_pid, mirror_id, host, token, data=data)
                    if resp.status_code != 200:
                        self.log.error(
                            f"Failed to {'disable' if disable else 'enable'} project {dst_pid} push mirror to {mirror_path}, with response:\n{resp} - {resp.text}")
            except RequestException as re:
                self.log.error(
                    f"Failed to toggle project {s.get('path_with_namespace')} push mirror, with error:\n{re}")
                continue
        add_post_migration_stats(start, log=self.log)

    def find_mirror_project(self, staged_project, host, token):
        try:
            orig_path = get_dst_path_with_namespace(
                staged_project, mirror=True)
            orig_pid = self.find_project_by_path(host, token, orig_path)
            if not orig_pid:
                self.log.error(f"SKIP: Original project {orig_path} NOT found")
                return (False, False)
            mirror_path = get_dst_path_with_namespace(staged_project)
            mirror_pid = self.find_project_by_path(
                host, token, mirror_path)
            if not mirror_pid:
                self.log.error(
                    f"SKIP: Mirror project {mirror_path} NOT found")
                return (orig_pid, False)
            return (orig_pid, mirror_path)
        except RequestException as re:
            self.log.error(
                f"Failed to find project {orig_path} and/or push mirror, with error:\n{re}")

    def create_staged_projects_fork_relation(self, dry_run=True):
        start = time()
        rotate_logs()
        staged_projects = get_staged_projects()
        host = self.config.destination_host
        token = self.config.destination_token
        for s in staged_projects:
            orig_project = s.get("forked_from_project")
            # Cannot validate destination user namespace without email
            if orig_project and not is_user_project(orig_project):
                try:
                    fork_path = get_dst_path_with_namespace(s)
                    fork_pid = self.find_project_by_path(
                        host, token, fork_path)
                    if not fork_pid:
                        self.log.error(
                            f"SKIP: Fork project {fork_path} NOT found")
                        continue
                    orig_path = get_dst_path_with_namespace(orig_project)
                    orig_pid = self.find_project_by_path(
                        host, token, orig_path)
                    if not orig_pid:
                        self.log.error(
                            f"SKIP: Fork {fork_path} forked from project {orig_path} NOT found")
                        continue
                    self.log.info(
                        f"{get_dry_log(dry_run)}Create forked from {orig_path} to {fork_path} project relation")
                    if not dry_run:
                        resp = self.projects_api.create_project_fork_relation(
                            fork_pid, orig_pid, host, token)
                        if resp.status_code != 201:
                            self.log.error(
                                f"Failed to create forked from {orig_path} to {fork_path} project relation, with response {resp} - {resp.text}")
                except RequestException as re:
                    self.log.error(
                        f"Failed to create project {s.get('path_with_namespace')} fork relation, with error:\n{re}")
                    continue
        add_post_migration_stats(start, log=self.log)
